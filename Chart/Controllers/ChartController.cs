﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Dapper;
using System.Data.SqlClient;
using System.Configuration;
using Newtonsoft.Json;

namespace Chart.Controllers
{
    public class ChartController : ApiController
    {
        
        public IHttpActionResult Get(int id)
        {
            string query = @"SELECT SUM(OrderDetail.Total) AS Total
                            FROM [Order] INNER JOIN
                            OrderDetail ON [Order].Id = OrderDetail.OrderId
                            WHERE OrderDate >= @fromdate and OrderDate <= @todate
                            GROUP BY OrderDate";

            List<dynamic> data;
            using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["DbCon"].ConnectionString.ToString()))
            {
                con.Open();
                data = con.Query(query, new { fromdate = Convert.ToDateTime("2014-04-23"), todate = DateTime.Now.Date }).ToList();
                con.Close();
            }
            

            return Ok(data);
        }

    }
}
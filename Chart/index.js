﻿/// <reference path="Scripts/angular.js" />

var app = angular.module('charts', ['googlechart']);

app.controller('MainCtrl', function ($scope, $http) {

    $scope.dayselect = 0;
    $scope.charttype = 'LineChart';

    $scope.chartbind = function () {
        $scope.chart.type = $scope.charttype;

        $http({ method: 'GET', url: '/api/Chart/' + $scope.dayselect }).
            success(function (data, status, headers, config) {
                $scope.chart.data = [];
                $scope.chart.data.push(['', 'Total']);
                for (var i = 0; i < data.length; i++) {
                    $scope.chart.data.push(['', data[i].Total]);
                }
            }).
            error(function (data, status, headers, config) {

            });
    }

    var saleschart = {};
    saleschart.options = {
        title: "Total per day",
        isStacked: "false",
        displayExactValues: true,
        vAxis: {
            title: "Total",
            gridlines: {
                count: 6
            }
        },
        hAxis: {
            title: "Per Day"
        }
    };

    $scope.chart = saleschart;
});
